#!/usr/bin/python3

import pandas
import json
import argparse
import tabulate
import os

divider = '-'*100

def xl2json(filename):
	'''
	convert xlsx file at filename into a json dict
	from https://stackoverflow.com/questions/60044233/converting-excel-into-json-using-python
	'''
	excel_data_df = pandas.read_excel(filename)
	thisisjson = excel_data_df.to_json(orient='records')
	thisisjson_dict = json.loads(thisisjson)
	return thisisjson_dict

class ingredient:
	def __init__(self, jsonentry):
		self.json = jsonentry

	def getcfactor(self, unitstr):
		if unitstr in self.json['units']:
			units = str(self.json['units']).split(',')
			factors = str(self.json['conversion_factor']).split(',')
			if len(units) != len(factors):
				print('error: units and/or factors missing!:')
				print('ingredient:',str(self))
				print('units:',units)
				print('factors:',factors)
				exit(0)
			for i,j in enumerate(units):
				if j == unitstr:
					return float(factors[i])
		print(self.json)
		return -1

	def getmass(self, quantity, unitstr):
		'''
		returns the mass, in grams, of a given measurement of this ingredient,
			if a conversion is defined

		Parameters
		----------
		quantity : float
			quantity of the measurement

		unitstr : string
			unit of the quantity

		Returns
		----------
		float
			mass of the ingredient, in grams
		'''
		cfactor = self.getcfactor(unitstr)
		if cfactor != -1:
			return quantity*cfactor
		return -1

	def getservings(self, quantity, unitstr):
		'''
		returns the number of nutritional servings for the given quantity of
			the ingredient

		Parameters
		----------
		quantity : float
			quantity of the measurement

		unitstr : string
			unit of the quantity

		Returns
		----------
		float
			number of servings of the ingredient
		'''
		return self.getmass(quantity, unitstr)/self.json['serving_size']

	def todict(self):
		return self.json

	def __str__(self):
		return json.dumps(self.json)

class ingredients:
	def __init__(self, filename):
		jdict = xl2json(filename)
		self.filename = filename
		self.data = []
		for i in jdict:
			self.data.append(ingredient(i))

	def search(self, ingname):
		for i in self.data:
			if i.json['name'] == ingname:
				return i
		return -1

	def __str__(self):
		return self.filename.replace('.xlsx', '') + ':\n' + '\n'.join([str(i) for i in self.data])

	def todict(self):
		return [i.todict() for i in self.data]

class recipe:
	def __init__(self, ingredientsobj, recipefname, servings):
		self.name = recipefname.replace('.xlsx','').replace('.ods','')
		jsonarr = xl2json(recipefname)
		self.ingredients = []
		self.nutritioncomponents = []
		self.nutrition = {}
		self.nutrition['name'] = self.name
		self.nutrition['total_vals'] = {}
		self.nutrition['total_vals']['calories'] = 0
		self.nutrition['total_vals']['protein'] = 0
		self.nutrition['total_vals']['carbs'] = 0
		self.nutrition['total_vals']['fat'] = 0
		self.nutrition['total_vals']['mass'] = 0
		self.nutrition['total_vals']['mass_lbs'] = 0
		self.nutrition['servings'] = servings
		for i in jsonarr:
			#print('read line:',i)
			if i['name'] == None:
				break
			ing = ingredientsobj.search(i['name'])
			self.ingredients.append(ing)
			#print('ingredient found:',ing)
			if ing == -1:
				print('could not find ingredient:',i['name'])
				print('for recipe:',recipefname)
				exit(0)
			servs = ing.getservings(i['measurement'], i['units'])
			if servs < 0:
				print('could not get serving size:',i['name'])
				print('for recipe:',recipefname)
				exit(0)
			#print('servs:',servs)
			self.nutrition['total_vals']['calories'] += ing.json['calories']*servs
			self.nutrition['total_vals']['protein'] += ing.json['protein']*servs
			self.nutrition['total_vals']['carbs'] += ing.json['carbs']*servs
			self.nutrition['total_vals']['fat'] += ing.json['fat']*servs
			self.nutrition['total_vals']['mass'] += ing.json['serving_size']*servs
			self.nutrition['total_vals']['mass_lbs'] += ing.json['serving_size']*servs/453.592
			self.nutritioncomponents.append({
				'name': ing.json['name'],
				'calories': ing.json['calories']*servs,
				'protein': ing.json['protein']*servs,
				'carbs': ing.json['carbs']*servs,
				'fat': ing.json['fat']*servs,
				'mass': ing.json['serving_size']*servs,
				'servings': servs
				})
		self.nutrition['macro_calorie_factors'] = {
			'protein': (self.nutrition['total_vals']['protein']*4)/self.nutrition['total_vals']['calories'],
			'carbs': (self.nutrition['total_vals']['carbs']*4)/self.nutrition['total_vals']['calories'],
			'fat': (self.nutrition['total_vals']['fat']*4)/self.nutrition['total_vals']['calories']
		}
		self.nutrition['serving_vals'] = {}
		self.nutrition['serving_vals']['calories'] = self.nutrition['total_vals']['calories']/servings
		self.nutrition['serving_vals']['protein'] = self.nutrition['total_vals']['protein']/servings
		self.nutrition['serving_vals']['carbs'] = self.nutrition['total_vals']['carbs']/servings
		self.nutrition['serving_vals']['fat'] = self.nutrition['total_vals']['fat']/servings
		self.nutrition['serving_vals']['mass'] = self.nutrition['total_vals']['mass']/servings
		self.nutrition['serving_vals']['mass_ounces'] = self.nutrition['total_vals']['mass']/servings/28.25
		self.nutrition['serving_vals']['mass_pounds'] = self.nutrition['total_vals']['mass']/servings/453.592

	def todict(self):
		outp = self.nutrition
		outp['ingredients'] = self.nutritioncomponents
		return outp

	def __str__(self):
		dict = self.todict()
		#return '\n'.join([str(i) + ': ' + str(dict[i]) for i in dict])
		return json.dumps(dict, indent=4)

def testrecipe(ingredientsfname, recipefname):
	print('using ingredients file:', ingredientsfname)
	ings = ingredients(ingredientsfname)
	print(ings)
	r = recipe(ingredients(ingredientsfname), recipefname, 5)
	print(r)

def getproteincoefficient(ingredientjson):
	if float(ingredientjson['calories']) == 0:
		return 0
	caloriesfromother = (9*float(ingredientjson['fat'])) + \
		(4*float(ingredientjson['carbs']))
	if caloriesfromother == 0:
		if float(ingredientjson['protein']) == 0:
			return 0
		else:
			return 1000
	return (4*float(ingredientjson['protein']))/caloriesfromother

def getproteinperfat(ingredientjson):
	if float(ingredientjson['fat']) == 0:
		if float(ingredientjson['protein']) == 0:
			return 0
		else:
			return 1000
	return float(ingredientjson['protein'])/float(ingredientjson['fat'])

def getcalstomass(ingredientjson):
	if float(ingredientjson['calories']) == 0:
		if float(ingredientjson['serving_size']) > 0:
			return 1000
		else:
			return 0
	return float(ingredientjson['serving_size'])/float(ingredientjson['calories'])

#testrecipe('ingredients.xlsx', 'chili.xlsx')

def getprotein(rec):
	print('getprotein:',rec)
	return rec['macro_factors']['protein']

def mainfunc():
	ap = argparse.ArgumentParser('calculate macros for a given recipe')
	ap.add_argument('-p', '--protein', help='calculate the best source of protein for given number of ingredientss')
	ap.add_argument('-pf', '--proteinfat', help='calculate the best source of protein to minimize fat for given number of ingredients')
	ap.add_argument('-f', '--filler', help='calculate the best ingredients to use for filler')
	ap.add_argument('-r ', '--recipe', type=str, help='recipe xlsx file to analyze')
	ap.add_argument('-s', '--servings', type=int, help='number of servings to divide the recipe into')
	ap.add_argument('-pr', '--recipesprotein', help='list the specified number of recipes in order of increasing protein efficiency')
	args = ap.parse_args()
	ings = ingredients('ingredients.xlsx')
	if args.protein:
		ings2 = ings.todict()
		ings2.sort(key=getproteincoefficient, reverse=True)
		ings2 = ings2[:int(args.protein)]
		outp = []
		for i,j in enumerate(ings2):
			caloriesfromother = (9*float(j['fat'])) + \
				(4*float(j['carbs']))
			j = {
				'index': i+1,
				'name': j['name'],
				'proteincals': 4*float(j['protein']),
				'calsfromother': caloriesfromother,
				'efficiency': getproteincoefficient(j)
			}
			outp.append(j)
		print(tabulate.tabulate(outp, headers='keys'))
		exit(0)
	if args.proteinfat:
		ings2 = ings.todict()
		ings2.sort(key=getproteinperfat, reverse=True)
		ings2 = ings2[:int(args.proteinfat)]
		outp = []
		for i,j in enumerate(ings2):
			j = {
				'index': i+1,
				'name': j['name'],
				'protein': float(j['protein']),
				'fat': float(j['fat']),
				'efficiency': getproteinperfat(j)
			}
			outp.append(j)
		print(tabulate.tabulate(outp, headers='keys'))
		exit(0)
	if args.filler:
		ings2 = ings.todict()
		ings2.sort(key=getcalstomass, reverse=True)
		ings2 = ings2[:int(args.filler)]
		outp = []
		for i,j in enumerate(ings2):
			j = {
				'index': i+1,
				'name': j['name'],
				'mass': float(j['serving_size']),
				'calories': float(j['calories']),
				'efficiency (g/cal)': getcalstomass(j)
			}
			outp.append(j)
		print(tabulate.tabulate(outp, headers='keys'))
		exit(0)
	if args.recipesprotein:
		files = os.listdir('.')
		recipesfiles = []
		for i in files:
			if '.xlsx' or '.ods' in i and not 'ingredients' in i and not 'lock.' in i:
				recipesfiles.append(i)
		recipes = []
		for i in recipesfiles:
			try:
				recipes.append(recipe(ings, i, 1))
			except Exception as e:
				print('could not generate recipe for:',i)
				print(e)
		outp = []
		for i in recipes:
			j = i.todict()
			outp.append({'name': j['name'], 'macro_factors': j['macro_calorie_factors']})
		#outp2 = sorted(outp, key=getprotein)
		outp2 = sorted(outp, key=lambda a : a['macro_factors']['protein'], reverse=True)[:int(args.recipesprotein)]
		print(divider)
		print(tabulate.tabulate([[j, i['name']]+list(i['macro_factors'].values()) for j,i in enumerate(outp2)], headers=['#', 'name','protein','carbs','fat']))
		exit(0)

	r = recipe(ings, args.recipe, int(args.servings))
	print(r)

if __name__ == '__main__':
	mainfunc()

#print(xl2json('chili.xlsx'))
#calcrecipe('chili.xlsx')
